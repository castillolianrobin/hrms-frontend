import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('../views/Dashboard'),
    meta: { layout: 'User' }
  },
  
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/Profile'),
    meta: { layout: 'User' }
  },

  {
    path: '/mail',
    name: 'Messages',
    component: () => import('../views/Messages'),
    meta: { layout: 'User' }
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
