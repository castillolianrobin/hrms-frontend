import { createApp } from 'vue'
import App from './App.vue'
import './assets/tailwind.css'
import router from './router'
// import PrimeVue from 'primevue/config';
// const app = createApp(App);
// App.use(PrimeVue);

createApp(App).use(router).mount('#app')
